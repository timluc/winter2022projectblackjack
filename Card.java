public class Card {
  private Suit suit;
  private Value value;


public Card(Suit s, Value v) {
this.suit = s;
this.value = v;
}

public String getCard() {
	return this.value +" of "+ this.suit.toString();
}

public int getCardValue() {
return this.value.getValue();
}
}



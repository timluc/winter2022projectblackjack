import java.util.Scanner;
public class Test {
	public static void main(String args[]) {
	
	DynamicCardArray deck = new DynamicCardArray();
	DynamicCardArray playerHand = new DynamicCardArray();
	DynamicCardArray dealerHand = new DynamicCardArray();
	
	createGame(deck, playerHand, dealerHand);
	
	
	Scanner read = new Scanner(System.in);
	
	System.out.println("Do you wish to Hit(h) or Stand(s)?");
	String choice = read.nextLine(); 

	while(choice != "h " || choice != "s ") {
	System.out.println("Please Enter a valid option.");
	choice = read.nextLine();
	if(choice.equals("h") || choice.equals("s")){
	break;
	}
	}
	
	while(choice.equals("h")) {
	choice = hit(choice, deck, playerHand, read);
	int playerSum = playerHand.sumHand();
	if(playerHand.checkIfBusted(playerSum) == true) {
	break;
	}
	}

	if(choice.equals("s")){
	System.out.println("You have chosen to stand. It is now the dealer's turn.");
	dealerTurn(deck, playerHand, dealerHand);
	}
	if(playerHand.sumHand() <= 21) {
	checkWinner(playerHand, dealerHand);
	}
	}


public static void createGame(DynamicCardArray deck, DynamicCardArray playerHand, DynamicCardArray dealerHand) {
System.out.println("Welcome to BlackJack!");
System.out.println("The game is about to start.");
deck.createDeck();
	deck.shuffle();
	
	
	dealerHand.addCard(deck.removeCard());
	System.out.println("Here is one of the dealer's cards: " + dealerHand.getCardArray());
	dealerHand.addCard(deck.removeCard());
	
	playerHand.addCard(deck.removeCard());
	playerHand.addCard(deck.removeCard());
	System.out.println("Here are your cards: \n" + playerHand.getCardArray());
	System.out.println("The sum of your hand is: " + playerHand.sumHand());
}



public static String hit(String choice, DynamicCardArray deck, DynamicCardArray playerHand, Scanner read) {
	
	playerHand.addCard(deck.removeCard());
	System.out.println("This is your new hand: \n" + playerHand.getCardArray());
	System.out.println("The sum of your new hand is: " + playerHand.sumHand());
	if(playerHand.checkIfBusted(playerHand.sumHand()) == false){
	System.out.println("Do you still wish to Hit(h) or Stand(s)?");
	choice = read.nextLine();
	}
	else {
	System.out.println("You busted! The dealer won the game.");
	}
	 return choice;
	

}

public static void dealerTurn(DynamicCardArray deck, DynamicCardArray playerHand, DynamicCardArray dealerHand) {
	while(dealerHand.sumHand() < 16) {
		
	dealerHand.addCard(deck.removeCard());
	System.out.println("This is the dealer's hand after drawing. \n" + dealerHand.getCardArray());
	}
}

public static void checkWinner(DynamicCardArray playerHand, DynamicCardArray dealerHand) {
System.out.println("This is the sum of your hand: " + playerHand.sumHand());
System.out.println("\nThis is the dealer's hand: \n" + dealerHand.getCardArray());
System.out.println("This is the sum of the dealer's hand: " + dealerHand.sumHand());

if(playerHand.checkIfBusted(playerHand.sumHand()) == false && dealerHand.checkIfBusted(dealerHand.sumHand()) == false){
if(playerHand.sumHand() > dealerHand.sumHand()){
	System.out.println("You have won!!!");
}
else{
System.out.println("The dealer won!");
}
}
else {
System.out.println("The dealer has busted!");
}
}
}

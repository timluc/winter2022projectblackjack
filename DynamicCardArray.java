 import java.util.Random;
 public class DynamicCardArray {
  public Card[] cards;
  public int next;
	
	public DynamicCardArray() {
		this.cards = new Card[52]; 
		this.next = 0;
	}
	
public String getCardArray() {
String s = "";
for (int i = 0; i < this.next ; i++) {
	s = s + this.cards[i].getCard() +"\n";
}
return s; 

}

public Card removeCard() {
	Card storedCard = this.cards[0];
	for(int i = (this.cards.length - 1); i >= 0; i--){
	if(this.cards[i] != null){
	storedCard = this.cards[i];
	this.cards[i] = null;
	break;
	}
}
	return storedCard;
}

public void addCard(Card n) {
this.next++;
for(int i = 0; i < this.cards.length; i++) {
	if(this.cards[i] == null){
	this.cards[i] = n;
	break;
	}
	}
}

 public void shuffle() {
	Random rand = new Random();
	int times = 100;
	for (int i = 0; i < times ; i++) {
	int rand1 = rand.nextInt(this.next);
	int rand2 = rand.nextInt(this.next);
	Card[] storedposition = new Card[1];
	storedposition[0] = this.cards[rand2];
	this.cards[rand2] = this.cards[rand1];
	this.cards[rand1] = storedposition[0];
	}
	
} 

public void createDeck() { 
	int i = 0; 
	this.next = 52;
	for (Suit s : Suit.values()) {
	for (Value v : Value.values()){
		this.cards[i] = new Card(s,v);
		i++;
	}
  } 
 }
 
 
 public int sumHand(){
	int sum = 0;
	for(int i = 0; i < this.next; i++) {
		sum = sum + this.cards[i].getCardValue();
	}
 return sum;
 }
 
 public boolean checkIfBusted(int sum){
		 if(sum > 21) {
		 return true;
		 }
		 return false;
	 }
 
}
 